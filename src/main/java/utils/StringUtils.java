package utils;

import java.util.ArrayList;
import java.util.List;

public abstract class StringUtils {
	
	public static String getNextTerm (String str) {
		String result = "";
		
		for (char c : str.toCharArray()) {
			if (c != '(' && c != ')' && c != ',' && c != ' ') {
				result += c;
			} else break;
		}
		return result;
	}
	
	public static boolean isTerm (String expr, String term) {
		String t = "";
		for (char c : expr.toCharArray()) {
			if (c != '(' && c != ')' && c != ',') {
				t += c;
			} else {
				if (t.equals(term))
					return true;
				t = "";
			}
		}
		return false;
	}
}
