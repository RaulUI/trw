package utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import obj.Node;
import obj.Tree;
import obj.Type;

public abstract class TreeUtils {
	
	public static Tree stringToTree(String expression, String arity) {
//		System.out.println("Expression to analyse: " + expression);
//		System.out.println("Arity map: " + arity);
		
		Map<String, Integer> arityMap = getArityMap(arity);
		
		Node root = makeRoot(expression, arityMap);
		Stack<Node> insertedNodes = new Stack<Node>();
		insertedNodes.add(root);
				
		//Start from 1 because we already created the root
		for (int i = 1; i < expression.length(); i++) {

			String c = StringUtils.getNextTerm(expression.substring(i)).trim();
			
			if (c == "(" || c == ")" || c == "," || c.isEmpty()) continue;
			
			//If a term is formed of more chars
			if (c.length() > 1) i+=c.length()-1;
			
			Node n = new Node();
			n.setValue(c);
			n.setArity(0);
			
			if (arityMap.containsKey(String.valueOf(c))) {
				n.setArity(arityMap.get(String.valueOf(c)));

				//Case for only a constant or function
				if (expression.length() == 1 || expression.charAt(expression.indexOf(c)+1) != '(') {
					n.setType(Type.Constant);
				} else {
					n.setType(Type.Function);
				}
			} else {
				n.setType(Type.Variable);
			}
			
			if (insertedNodes.isEmpty())
				break;
			Node parent = Tree.getNodeForInsertion(insertedNodes.get(insertedNodes.size()-1));
			
			if (n.getArity() > 0)
				insertedNodes.add(n);
			
			parent.addChildren(n);
			
			if(parent.getArity() == parent.getChildren().size()) {
				insertedNodes.remove(parent);
			}
		}
		
		//TODO: Add a check of the arity map with regards to the tree
		
		return new Tree(root);
	}
	
	private static Node makeRoot (String expression, Map<String, Integer> arityMap) {
		Node root = new Node();
		char c = expression.charAt(0);
		
		root.setValue(c);
		root.setArity(0);
		
		if (arityMap.containsKey(String.valueOf(c))) {
			root.setArity(arityMap.get(String.valueOf(c)));

			//Case for only a constant or function
			if (expression.length() == 1 || expression.charAt(1) != '(') {
				root.setType(Type.Constant);
			} else {
				root.setType(Type.Function);
			}
		} else {
			root.setType(Type.Variable);
		}
		
		return root;
	}
	
	public static Map<String, Integer> getArityMap (String arity) {
		Map<String, Integer> arrityMap = new HashMap<String, Integer>();
		arity = arity.substring(1, arity.length()-1);
		
		String [] arr = arity.split(";");
		for (String s : arr) {
			s = s.trim();
			try {
				String key = s.split("=")[0];
				Integer value = Integer.parseInt(s.split("=")[1]);
				arrityMap.put(key, value);
			} catch (IndexOutOfBoundsException iobe) {
				System.out.println("Arrity string is ill formated!");
			} catch (Exception e) {
				System.out.println("Error while creating the arrity map!");
			}
		}
		
		return arrityMap;
	}
	
	public static String treeToString (Node n) {
		String str = treeToStringR(n);
		
		if (str.length() > 1) str = str.substring(0, str.length()-1);
		return str;
	}

	static List<Integer> ar = new ArrayList<Integer>();
	
	private static String treeToStringR (Node n) {
		String str = "";
		if (n != null) {
			str += n.getValue().toString();
			if (n.getType() == Type.Function) {
				ar.add(n.getArity());
				str += "(";
			} else {
				if (ar.size() == 0) return n.getValue().toString();
				int lastElement = ar.size()-1;
				ar.set(lastElement, ar.get(lastElement)-1);
				while (ar.get(lastElement) == 0) {
					ar.remove(lastElement);
					str += ")";
					if (ar.size() == 0)
						break;
					lastElement = ar.size()-1;
					ar.set(lastElement, ar.get(lastElement)-1);
				}
				str += ",";
			}
			
			
			if (n.getChildren() == null)
				return "";
			for (Node node : n.getChildren()) {
				str += treeToStringR(node);
			}
		}
		return str;
	}
	
	public static void replaceChild (Node parent, Node oldChild, Node newChild) {
		if (parent.getChildren() == null) {
			System.out.println("Parent does not have children");
			return;
		}
		if (!parent.getChildren().contains(oldChild) || parent.getChildren().contains(newChild)) {
//			System.out.println("Old child does not exist or new child already there.");
			return;
		}
		oldChild.setParent(null);
		parent.getChildren().remove(oldChild);
		parent.addChildren(newChild);
	}
	
	public static boolean checkChildren (Node n1, Node n2) {
		if (n1.getChildren().size() != n2.getChildren().size()) {
			return false;
		} 
		
		for (int i = 0; i < n1.getChildren().size(); i++) {
			if (!n1.getChildren().get(i).getValue().equals(n2.getChildren().get(i).getValue()))
				return false;
		}
		return true;
	}
	
	public static boolean isChildOf (Node child, Node parent) {
		for (Node n : parent.getChildren()) {
			if (child.getValue().toString().equals(n.getValue().toString()))
				return true;
		}
		return false;
	}
	
	public static boolean contains (Node root, Node n) {
		if (root != null) {
			if (root.getValue().toString().equals(n.getValue().toString()))
				return true;
			if (root.getChildren() == null)
				return false;
			for (Node node : root.getChildren()) {
				if (contains(node, n)) return true;
			}
		}
		return false;
	}
	
}
