package algo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import obj.Node;
import obj.Tree;
import obj.Type;
import utils.TreeUtils;

public abstract class Unifier {

	public static void example () {
//		String term1 = "P(f(x),y,g(y))";
//		String term2 = "P(f(x),z,g(x))";
//		String arity1 = "{P=3;f=1;g=1}";
//		String arity2 = "{P=3;f=1;g=1}";
		
//		String term1 = "P(x, B, B)";
//		String term2 = "P(A, y, z)";
//		String arity1 = "{P=3;B=0}";
//		String arity2 = "{P=3;A=0}";
		
//		String term1 = "P(g(f(v)),g(u))";
//		String term2 = "P(x, x)";
//		String arity1 = "{P=2;g=1;f=1}";
//		String arity2 = "{P=2}";
		
//		String term1 = "P(A, x, f(g(y)))";
//		String term2 = "P(y, f(z), f(z))";
//		String arity1 = "{P=3;A=0;f=1;g=1}";
//		String arity2 = "{P=3;f=1}";
		
		String term1 = "P(x,g(f(A)),f(x))";
		String term2 = "P(f(y),z,y)";
		String arity1 = "{P=3;g=1;f=1;A=0}";
		String arity2 = "{P=3;f=1}";
		
//		String term1 = "P(x, f(y))";
//		String term2 = "P(z, g(w))";
//		String arity1 = "{P=2;f=1}";
//		String arity2 = "{P=2;g=1}";
		
		System.out.println("\nUNIFIER (GNU)\n");
		
		System.out.println("Expression 1: " + term1);
		System.out.println("Expression 2: " + term2);
		Tree t1 = TreeUtils.stringToTree(term1, arity1);
		Tree t2 = TreeUtils.stringToTree(term2, arity2);
		List<Node> lt1 = new ArrayList<Node>();
		lt1.add(t1.root);
		List<Node> lt2 = new ArrayList<Node>();
		lt2.add(t2.root);
		
		Map<Node, Node> subst = unify(lt1, lt2, new HashMap<Node, Node>());
		System.out.println("Unification: " + printMap(subst));
	}
	
	private static Map<Node, Node> unify (List<Node> lt1, List<Node> lt2, Map<Node, Node> s) {
		if (s == null) {
			return null;
		}
		
		Node term1 = null, term2 = null;
		if (lt1.size() == 1 && lt2.size() == 1) {
			term1 = lt1.get(0);
			term2 = lt2.get(0);
		} else {
			return unify(lt1.subList(1, lt1.size()),
					lt2.subList(1, lt1.size()),
					unify(lt1.subList(0, 1), lt2.subList(0, 1), s));
		}
		
		if (TreeUtils.treeToString(term1).equals(TreeUtils.treeToString(term2))) {
			return s;
		} else if (term1.getType() == Type.Variable) {
			return unifyVar(term1, term2, s);
		} else if (term2.getType() == Type.Variable) {
			return unifyVar(term2, term1, s);
		} else if (term1.getType() == Type.Constant || term1.getType() == Type.Function) {
			if (term1.getType() == term2
					.getType() && term1.getValue().toString().equals(term2.getValue().toString())) {
				return unify(term1.getChildren(), term2.getChildren(), s);
			} else {
				return null;
			}
		}
		
		return null;
	}

	private static Map<Node, Node> unifyVar(Node t1, Node t2, Map<Node, Node> s) {
		if (containsKey(t1, s)) {
			
			List<Node> lt2 = new ArrayList<Node>();
			lt2.add(t2);

			List<Node> lt1 = new ArrayList<Node>();
			lt1.add(getByValue(t1, s));
			
			return unify(lt1, lt2, s);
			
		} else if (containsKey(t2, s)) {
		
			return unifyVar(t1, getByValue(t2, s), s);		
			
		} else if (!checkOccurence(t1, t2, s)) {
			
			s.put(t1, t2);
			return s;
		}
		return s;
	}
	
	private static boolean containsKey (Node n, Map<Node, Node> s) {
		if (s == null) return false;
		for (Entry<Node, Node> entry : s.entrySet()) {
			if (entry.getKey().getValue().toString().equals(n.getValue().toString()))
				return true;
		}
		return false;
	}
	
	private static Node getByValue (Node n, Map<Node, Node> s) {
		if (s == null) return null;
		for (Entry<Node, Node> entry : s.entrySet()) {
			if (entry.getKey().getValue().toString().equals(n.getValue().toString()))
				return entry.getValue();
		}
		return null;
	}
	
	private static boolean checkOccurence (Node t1, Node t2, Map<Node, Node> s) {

		if (s == null) return false;
		
		if (TreeUtils.contains(t2, t1)) {
			return true;
		}
		
		if (s.containsKey(t1))
			return true;

		return false;
	}
	
	private static String printMap (Map<Node, Node> s) {
		if (s == null) return "None";
		String result = "{";
		for (Entry<Node, Node> entry : s.entrySet()) {
			result += TreeUtils.treeToString(entry.getKey()) + "/" + TreeUtils.treeToString(entry.getValue()) + ", ";
		}
		result = result.substring(0, result.length()-2);
		result += "}";
		return result;
	}
}
