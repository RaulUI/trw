package algo;

import java.util.HashMap;
import java.util.Map;

import utils.StringUtils;

public abstract class Substitution {

	public static void example () {
//		String expression = "f(xa)";
//		String substitutions = "{x=g(x,y,z); xa=g(TT,UU,LL)}";
		String expression = "f(xa,f(i(f(x,f(H,j))),e(i(x),Y)))";
		String substitutions = "{x=g(x,y,z); xa=g(TT,UU,LL); Y=i(H); j=e(Y,f(x,y,z))}";
		
		System.out.println("\nSUBSTITUTION");
		System.out.println("\nExpression to be substituted: " + expression);
		System.out.println("Substitutions: " + substitutions);
		String substituted = basic(expression, substitutions);
		System.out.println("Result: " + substituted);
		
	}
	
	public static void compositionExample () {
		String sigma = "{x=f(x,y); y=z; z=i(x)}";
		String theta = "{u=i(e); x=i(y); z=y}";

		System.out.println("\nCOMPOSITION of SUBSTITUTION");
		System.out.println("\nSigma to be substituted: " + sigma);
		System.out.println("Theta to be substituted: " + theta);
		
		composition(sigma, theta);
	}
	
	private static String basic (String expression, String substitutions)  {
		Map<String, String> subst = getSubstitutionMap(substitutions);
		
		String result = "";
		while (!expression.isEmpty()) {
			String term = StringUtils.getNextTerm(expression);
			if (term.length() != 0) {
				//Case we find a term in the map, we add the value of the subst
				if (subst.containsKey(term)) {
					result += subst.get(term);
				//We don't find the term in the map, we add the key(term itself)
				} else {
					result += term;
				}
				expression = expression.substring(term.length());
			} else {
				//This case represents a token
				result += expression.charAt(0);
				expression = expression.substring(1);
			}
		}
		return result;
	}
	
	private static void composition (String sigmaSubst, String thetaSubs) {
		Map<String, String> sigma = getSubstitutionMap(sigmaSubst);
		Map<String, String> theta = getSubstitutionMap(thetaSubs);
		Map<String, String> result = new HashMap<String, String>();

		for (Map.Entry<String,String> entry : theta.entrySet()) {
			String str = entry.getValue();
			while (!str.isEmpty()) {
				String term = StringUtils.getNextTerm(str);
				if (term.length() != 0) {
					if (sigma.containsKey(term)) {
						result.put(entry.getKey(), entry.getValue().replace(term, sigma.get(term)));
					}
					str = str.substring(term.length());
				} else {
					str = str.substring(1);
				}
				//Check for remaining terms that were not substituted in theta
				if (!result.keySet().contains(entry.getKey())) {
					result.put(entry.getKey(), entry.getValue());
				}
			}
		}

		//Check for remaining terms that were not substituted in sigma
		for (Map.Entry<String,String> entry : sigma.entrySet()) {
			if (!result.keySet().contains(entry.getKey())) {
				result.put(entry.getKey(), entry.getValue());
			}
		}
		
		//Check for collapses
		System.out.print("\nCollapses: ");
		for (Map.Entry<String,String> entry : result.entrySet()) {
			if (entry.getKey().equals(entry.getValue())) {
				System.out.print(entry.getKey() + "->" + entry.getValue() + "  ");
				result.remove(entry.getKey());
			}
		}
		System.out.println("\nResult of composition Theta(Sigma): " + result);
	}
	
	private static Map<String, String> getSubstitutionMap (String subst) {
		Map<String, String> substitutions = new HashMap<String, String>();
		
		subst = subst.substring(1, subst.length()-1);
		
		String [] arr = subst.split(";");
		for (String s : arr) {
			s = s.trim();
			try {
				String key = s.split("=")[0];
				String value = s.split("=")[1];
				substitutions.put(key, value);
			} catch (IndexOutOfBoundsException iobe) {
				System.out.println("Arrity string is ill formated!");
			} catch (Exception e) {
				System.out.println("Error while creating the arrity map!");
			}
		}
		
		return substitutions;
	}
	
}
