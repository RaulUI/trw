package algo;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import obj.Node;
import obj.Tree;
import obj.Type;
import utils.TreeUtils;

public abstract class Matching {
	static LinkedHashMap <Node, Node> match = new LinkedHashMap<Node, Node> ();
	static List<String> subst = new LinkedList<String>();
	static List<Node> toRemove = new LinkedList<Node>();
	static boolean stop = false, reset = false;
	
	public static void example () {
//		String term1 = "f(1,x)";
//		String term2 = "f(1,f(y,x))";
//		String arity1 = "{f=2;1=0}";
//		String arity2 = "{f=2;1=0}";
		String term1 = "f(q(a,w),z)";
		String term2 = "f(q(g(x),y),t(b,tt,h))";
		String arity1 = "{f=2;q=2}";
		String arity2 = "{f=2;g=1;t=3;q=2}";
		
		System.out.println("\nMATCHING");
		System.out.println("\nExpression: " + term1);
		System.out.println("Object: " + term2);

		Tree t1 = TreeUtils.stringToTree(term1, arity1);
		Tree t2 = TreeUtils.stringToTree(term2, arity2);
		
		match.put(t1.root, t2.root);
		//TODO: Add a stopping mechanism if there is no solution
		while (!match.isEmpty() && !stop) {
			toRemove = new LinkedList<Node>();
			reset = false;
			
			for (Entry<Node, Node> entry : match.entrySet()) {
				match (entry.getKey(), entry.getValue());
				if (stop || reset)
					break;
			}
			
			for (Node n : toRemove) {
				match.remove(n);
			}
		}
		System.out.println("Found following substitutions: " + subst);
	}
	
	private static void match (Node term1, Node term2) {
		if (match.isEmpty()) {
			System.out.println("Final");
			return;
		}
		
		if (term1.getType() == Type.Variable) {
			if (term2.getType() == Type.Function) {
//				if (TreeUtils.isChildOf(term1, term2)) return;
				String func = TreeUtils.treeToString(term2);
				System.out.println("Found variable " + term1.getValue() + " to be substituted by function " + func);
				subst.add(term1.getValue() + "=" + func);
			} else {
				System.out.println("Found variable " + term1.getValue() + " to be substituted by constant " + term2.getValue());
				subst.add(term1.getValue() + "=" + term2.getValue());
			}
			TreeUtils.replaceChild(term1.getParent(), term1, term2);
			toRemove.add(term1);
		} else if ( (term1.getType() == Type.Function || term1.getType() == Type.Constant) && term2.getType() == Type.Variable) {
			stop = true;
			System.out.println("Matching FAILED, cannot match function " + term1.getValue() + " with variable " + term2.getValue());
			return;
		} else if (term1.getType().equals(term2.getType())) {
			if (term1.getValue().equals(term2.getValue())) {				
				if (term1.getType() == Type.Function) {
					if (TreeUtils.checkChildren(term1, term2)) {
						System.out.println("Function has the same terms as parameters.");
						toRemove.add(term1);
						return;
					}
					System.out.println("Same function named " + term1.getValue() + " so we analize its children:\n[" + term1.getChildren() + "]");
					for (int i = term1.getChildren().size()-1; i >= 0; i--) {
						addFirst(term1.getChildren().get(i), term2.getChildren().get(i));
					}
				} else {
					System.out.println("Same constant " + term1.getValue() + " so we match it.");
					toRemove.add(term1);
				}
			}
		}
	}
	
	private static void addFirst (Node key, Node value) {
		@SuppressWarnings("unchecked")
		LinkedHashMap<Node, Node> newmap = (LinkedHashMap<Node, Node>) match.clone();
		match.clear();
		if (!newmap.containsKey(key)) {
			reset = true;
			match.putIfAbsent(key, value);
		}
		match.putAll(newmap);
	}
	
}
