package algo;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections4.iterators.PermutationIterator;

import obj.Node;
import obj.Tree;
import obj.Type;
import utils.TreeUtils;

public abstract class Termination {

	private static String arity = "{M=2;S=1;A=2}";
	
	public static void example () {

		System.out.println("\nTERMINATION\n");

		String [] lpos = {"f(f(x,y),z)->f(x,f(y,z))", "f(i(x),x)->1", "f(1,x)->x"};
		arity="{f=2;i=1;1=0}";
		
		for (String precedence : getAllPrecedence(arity)) {
			String precedenceStr = precedence.replace(";", "<");
			System.out.println("Starting test for precedence " + precedenceStr);
			boolean check = true;
			for (String lpo : lpos) {
				String left = lpo.split("->")[0];
				String right = lpo.split("->")[1];
				if (lpo(left, right, precedence)) {
					System.out.println("Check passed for " + lpo + "\n");
				} else if (lpo(right, left, precedence)) {
					System.out.println("Check passed for " + lpo + "\n");
				} else {
					check = false;
					System.out.println("Check failed for " + lpo + "\n");
				}
			}
			if (check) {
				System.out.println("PRECEDENCE " + precedenceStr + " IS VALID FOR TERMINATION!\n");
			} else {
				System.out.println("Precedence " + precedenceStr + " is not valid for termination!\n");
			}
		}
	}
	
	public static void exampleLPO () {
		System.out.println("\nTERMINATION LPO\n");
		
		String lpo = "M(x,S(y))->A(x,M(x,y))";
		String precedence = "{A=1;S=0;M=2}";

		String left = lpo.split("->")[0];
		String right = lpo.split("->")[1];
		if (lpo(left, right, precedence)) {
			System.out.println("Check passed!");
		} else {
			System.out.println("Check failed!");
		}
	}
	
	private static boolean lpo(String left, String right, String precedence) {
		Map<String, Integer> prec = TreeUtils.getArityMap(precedence);
//		System.out.println("Left: " + left);
//		System.out.println("Right: " + right);
		Tree leftT = TreeUtils.stringToTree(left, arity);
		Tree rightT = TreeUtils.stringToTree(right, arity);
		
		//Case LP01
		if (leftT.root.getType() != Type.Variable && rightT.root.getType() == Type.Variable) {
			System.out.print("LP01 on " + left + " and " + right);
//			if (TreeUtils.isChildOf(rightT.root, leftT.root))
			if (TreeUtils.contains(leftT.root, rightT.root)) {
				System.out.println(" | " + right + " is contained in left expression so it holds.");
				return true;
			} else
				return false;
		}
		
		//Case LP02
		if (leftT.root.getType() != Type.Variable && rightT.root.getType() != Type.Variable) {
			System.out.print("LP02");
			int precVal = checkPrecedence(leftT.root, rightT.root, prec);
			if (precVal == 0 || precVal == 1) {
				
				if (precVal == 0) System.out.println("C on " + left + " and " + right);
				else System.out.println("B on " + left + " and " + right);
				
				//Both b and c have the same condition to check s >lpo t(j)
				for (Node child : rightT.root.getChildren()) {
					if (!lpo(left, TreeUtils.treeToString(child), precedence)) {
						return false;
					}
				}
				return true;
			} else if (precVal == 0) {
				//Case LP02c extra conditions
				for (int i = 0; i < leftT.root.getChildren().size(); i++) {
					if (lpo(TreeUtils.treeToString(leftT.root.getChildren().get(i)), TreeUtils.treeToString(rightT.root.getChildren().get(i)), precedence)) {
//						for (int j = 0; j < i; j++) {
//							if (!TreeUtils.treeToString(leftT.root.getChildren().get(j)).equals(TreeUtils.treeToString(rightT.root.getChildren().get(j)))) {
//								return false;
//							}
//						}
						return true;
					}
				}
				
			} else if (precVal == -1) {
				
				System.out.println("\nFailed because of the check of precedende!");
				return false;
				
			} else {
				//Case LP02a
				System.out.println("A on " + left + " | " + right);
				for (Node child : leftT.root.getChildren()) {
					if (TreeUtils.treeToString(child).equals(right)) {
						return true;
					}
					if (lpo(TreeUtils.treeToString(child), right, precedence)) {
						return true;
					}
				}				
			}
		}
		
		return false;
	}
	
	private static int checkPrecedence (Node n1, Node n2, Map<String, Integer> procedence) {
		String v1 = n1.getValue().toString();
		String v2 = n2.getValue().toString();
		
		if (!procedence.containsKey(v1) || !procedence.containsKey(v2))
			return -1;
		if (procedence.get(v1) == procedence.get(v2))
			return 0;
		else if (procedence.get(v1) > procedence.get(v2))
			return 1;
		return -2;
	}
	
	private static List<String> getAllPrecedence (String arity) {
		Set<String> terms = TreeUtils.getArityMap(arity).keySet();

		List<String> precedence = new ArrayList<String>();
		PermutationIterator<String> gen = new PermutationIterator<String>(terms);
		while (gen.hasNext()) {
	        List<String> perm = gen.next();
	        String prec = "{";
	        for (String p : perm) {
	        	prec += p + "=" + perm.indexOf(p) + ";";
	        }
	        prec = prec.substring(0, prec.length()-1);
	        prec += "}";
	        precedence.add(prec);
	    }
		return precedence;
	}
}
