package main;

import java.util.ArrayList;
import java.util.List;

import algo.Matching;
import algo.Substitution;
import algo.Termination;
import algo.Unifier;
import obj.Node;
import obj.Tree;
import utils.TreeUtils;

public class TermRewrite {

	public static void main(String[] args) {
		Node root = new Node("root");
		root.setPosition("");
		Node l = new Node("l");
		Node r = new Node("r");
		List<Node> c1 = new ArrayList<Node>();
		c1.add(l);
		c1.add(r);
		root.setChildren(c1);
		
		Node l1 = new Node("l1");
		Node l2 = new Node("l2");
		Node l3 = new Node("l3");
		List<Node> c2 = new ArrayList<Node>();
		c2.add(l1);
		c2.add(l2);
		c2.add(l3);
		l.setChildren(c2);
		
		Node r1 = new Node("r1");
		Node r2 = new Node("r2");
		Node r3 = new Node("r3");
		Node r4 = new Node("r4");
		List<Node> c3 = new ArrayList<Node>();
		c3.add(r1);
		c3.add(r2);
		c3.add(r3);
		c3.add(r4);
		r.setChildren(c3);
		
//		Tree test = new Tree(root);
//		t.print(root);
		
//		String expression = "f(x,f(g(i(y),z,f(x,y)),c))";
//		String arity = "{c=0; f=2; g=3; i=1}";
		String expression = "f(xa,f(i(f(x,f(H,j))),e(i(x),Y)))";
		String arity = "{f=2;i=1;e=2}";
		
		Tree t = TreeUtils.stringToTree(expression, arity);
		System.out.println("\nTree built from the expression:");
		t.print(t.root);
		System.out.println("\nExpression built from the tree:");
		System.out.println(TreeUtils.treeToString(t.root));
//		t.print(t.root);
		
		Substitution.example();
		Substitution.compositionExample();
		Matching.example();
		Unifier.example();
		Termination.exampleLPO();
		Termination.example();
	}

}
