package obj;

public class Tree {
	public Node root;
	
	public Tree(Node root) {
		this.root = root;
	}

	public Tree() {	}
	
	public void print (Node n) {
		if (n != null) {
			System.out.println(n);
			if (n.getChildren() == null)
				return;
			for (Node node : n.getChildren()) {
				print(node);
			}
		}
	}
	
	@SuppressWarnings("unused")
	public static Node getLastNode (Node n) {
		if(n == null) {
	        return null;
	    }
		
		if (n.getChildren().size() == 0) {
			return n;
		}
		
		Node right = n.getChildren().get(n.getChildren().size()-1);
	    while(right != null && n.getChildren().size() > 0) {
	    	right = n.getChildren().get(n.getChildren().size()-1);
	        n = right;
	    }
	    return n;
	}
	
	public static Node getNodeForInsertion (Node n) {
		if (n != null) {
			if (n.getChildren().size() < n.getArity()) {
				return n;
			}
			for (Node node : n.getChildren()) {
				getNodeForInsertion(node.getParent().getParent());
			}
		}
		return n;
	}
	
	public String toString () {
		return root.toString();
	}
}
