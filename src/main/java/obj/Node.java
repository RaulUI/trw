package obj;

import java.util.ArrayList;
import java.util.List;

public class Node {
	private Object value;
	private List<Node> children;
	private Node parent;
	private Type type;
	private String position;
	private int arity;

	public Node () {}
	
	public Node (Object v) {
		this.setValue(v);
	}
	
	Node (Object v, List<Node> children) {
		this.setValue(v);
		this.setChildren(children);
	}
	
	public Node (Node n) {
		this.setValue(n.getValue());
		this.setParent(n.getParent());
	}

	public Node getParent() {
		return parent;
	}

	public void setParent(Node parent) {
//		parent.getChildren().add(this);
		this.parent = parent;
	}

	public List<Node> getChildren() {
		if (children == null) {
			return new ArrayList<Node>();
		}
		return children;
	}

	public void setChildren(List<Node> children) {
		if (!children.isEmpty()) {
			for (Node n : children) {
				n.setParent(this);
				n.setPosition(n.getParent().getPosition() + (children.indexOf(n)+1));
			}
			this.children = children;
		}
	}

	public void addChildren(Node child) {
		child.setParent(this);
		if (children == null || children.isEmpty()) {
			children = new ArrayList<Node>();
		}
		children.add(child);
		child.setPosition(child.getParent().getPosition() + (children.indexOf(child)+1));
	}
	
	public String getPosition() {
		if (position == null)
			return "";
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

	public int getArity() {
		return arity;
	}

	public void setArity(int arrity) {
		this.arity = arrity;
	}
	
	public String getChildrenString () {
		if (children == null || children.isEmpty())
			return "";
		
		String str = "";
		for (Node n : children) {
			str += n.getValue() + n.getPosition() + ", ";
		}
		
		return  str.substring(0, str.length()-2);
	}
	
	public String getParentString () {
		if (parent == null) {
			return "";
		}
		return parent.getValue() + parent.getPosition();
	}
	
	public String toString() {
		return "Value=" + getValue() + " | position=" + getPosition() + " | arity=" + getArity() + " | parent=" + getParentString() + " | type=" + type + " | children=[" + getChildrenString() + "]";
	}
}
